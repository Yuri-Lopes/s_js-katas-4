const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";

const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];

const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function writeElement(id, valor) {
    let nameElement = document.createElement('div');
    nameElement.textContent = id;
    document.body.appendChild(nameElement);
    //cria uma div para guardar os valores de id, dos katas; se penduram no corpo de body
    
    let element = document.createElement('div');
    element.textContent = JSON.stringify(valor);
    document.body.appendChild(element)
    // a mesma ideia so que para guardar os valores de "valor" '' '' '' '' '' ''' '' ' ' ' '.
}
// Escreva uma função que retorna um array com as cidades em 'gotCitiesCSV'. 
//Lembre-se de também adicionar os resultados à página.

function kata1() {
    let id = 'kata 1';
    let  valor = gotCitiesCSV.split(',');
    
    writeElement(id, valor);
 return JSON.stringify(valor);
}
kata1();

// Escreva uma função que retorna um array das palavras na frase contida em 'bestThing'. 
//Lembre-se de também adicionar os resultados à página.
function kata2() {
    let id = 'kata 2';
    let valor= bestThing.split(',');
    //mostra pelas separaçoes "  "
    writeElement(id,valor);
    return JSON.stringify(valor);
}
kata2();



// Escreva uma função que retorna uma string separada por ponto-e-vírgulas em vez das 
//vírgulas de 'gotCitiesCSV'. Lembre-se de também adicionar os resultados à página.

function kata3() {
    let id = 'kata 3';
    let valor= gotCitiesCSV.replace(/,/g, ";");
    writeElement(id,valor);
    return JSON.stringify(valor);
    
    // for (let i = 0; i<gotCitiesCSV.length; i++) {
    //     if  (gotCitiesCSV[i] == ","){ 
    //         gotCitiesCSV [i] = ";";
        
    
}
kata3();


// Escreva uma função que retorne uma string CSV (separada por vírgulas) de 
//'lotrCitiesArray'. Lembre-se de também adicionar os resultados à página.

function kata4() {
    let id = 'kata 4';
    let valor = lotrCitiesArray.toLocaleString();
    //ignora ""
    writeElement(id,valor);
    return JSON.stringify(valor);
    
    //[] = ""
    // for (let i=0; i<lotrCitiesArray; i++) {
    //     if (lotrCitiesArray[i] == )
    // }

}
kata4();
// Escreva uma função que retorna um array com as 5 primeiras cidades de 'lotrCitiesArray'.
// Lembre-se de também adicionar os resultados à página.

function kata5() {
    let id ='kata 5';
    let valor = lotrCitiesArray.slice(0,5);
    //condição de inicio e parada
    writeElement(id,valor);
    return JSON.stringify(valor);
}
kata5();

// Escreva uma função que retorna um array com as 5 últimas cidades de 'lotrCitiesArray'. 
//Lembre-se de também adicionar os resultados à página.

function kata6() {
    let id = 'kata 6';
    let valor = lotrCitiesArray.slice(-5);
    //- traz de forma reversa
    writeElement(id,valor);
    return JSON.stringify(valor);
}
kata6();


// Escreva uma função que retorna um array contendo da 3ª a 5ª cidades de 'lotrCitiesArray'. 
//Lembre-se de também adicionar os resultados à página.
function kata7() {
    let id = 'kata 7';
    let valor = lotrCitiesArray.slice(2, 5);
    //inicio parada
    writeElement(id,valor);
    return JSON.stringify(valor);
}
kata7();

// Escreva uma função que use 'splice' para remover 'Rohan' de 'lotrCitiesArray' e 
//retorne o novo 'lotrCitiesArray' modificado. Lembre-se de também adicionar os resultados à página.
function kata8() {
    let id = 'kata 8';
    //copia do valor da constante []
    lotrCitiesArray.splice(2,1);
    //remove 
    writeElement(id,lotrCitiesArray);
    return JSON.stringify(lotrCitiesArray);
}
kata8();


// Escreva uma função que use 'splice' para remover todas as cidades depois de 
//'Dead Marshes' de 'lotrCitiesArray' e retorne o novo 'lotrCitiesArray' modificado. Lembre-se de também adicionar os resultados à página.

function kata9() {
    let id = 'kata 9';
   lotrCitiesArray.splice(5,);
    writeElement(id,lotrCitiesArray);
    return JSON.stringify(lotrCitiesArray);
}
kata9()


// Escreva uma função que use 'splice' para adicionar 'Rohan' de volta ao 
//'lotrCitiesArray' logo depois de 'Gondor' e retorne o novo 'lotrCitiesArray' modificado. Lembre-se de também adicionar os resultados à página.

function kata10() {
    let id = 'kata 10';
    lotrCitiesArray.splice(2,0,"Rohan");
    //arranca o 3 elemento e inicio de volta
    writeElement(id,lotrCitiesArray);
    return JSON.stringify(lotrCitiesArray);
}
kata10();


// Escreva uma função que use 'splice' para renomear 'Dead Marshes' para 'Deadest Marshes' 
//em 'lotrCitiesArray' e retorne o novo 'lotrCitiesArray' modificado. Lembre-se de também 
//adicionar os resultados à página.

function kata11() {
    let id = 'kata 11';
    lotrCitiesArray.splice(5,1,"Deadest Marshes");
    //arranca o elemento, inicia e colocando o elemento
    writeElement(id,lotrCitiesArray);
    return JSON.stringify(lotrCitiesArray);
}
kata11();


// Escreva uma função que usa 'slice' para retornar uma string com os primeiros 14 caracteres 
//de 'bestThing'. Lembre-se de também adicionar os resultados à página.

function kata12() {
    let id = 'kata 12';
    let valor = bestThing.slice(0,15);
    return JSON.stringify(valor);
}
kata12();

// Escreva uma função que usa 'slice' para retornar uma string com os 12 últimos caracteres de 
//'bestThing'. Lembre-se de também adicionar os resultados à página.

function kata13() {
    let id = 'kata 13';
    let valor = bestThing.slice(-13)
    writeElement(id,valor);
    return JSON.stringify(valor);
}
kata13();
// Escreva uma função que usa 'slice' para retornar uma string com os caracteres entre as 
//posições 23 e 38 de 'bestThing' (ou seja, 'booleano é par'). Lembre-se de também adicionar os 
//resultados à página.

function kata14() {
    let id = 'kata 14';
    let valor = bestThing.slice(23,38);
    writeElement(id,valor);
    return JSON.stringify(valor);
}
kata14();


// Escreva uma função que faz exatamente a mesma coisa que a #13 mas use o método 'substring' em 
//vez de 'slice'. Lembre-se de também adicionar os resultados à página.

function kata15() {
    let id = 'kata 15';
    let valor = bestThing.substring(bestThing.length-12);
    writeElement(id,valor);
    return JSON.stringify(valor);
}
kata15();



// Escreva uma função que faça exatamente a mesma coisa que o #14 mas use o método 'substring' em vez 
//de 'slice'. Lembre-se de também adicionar os resultados à página.

function kata16() {
    let id = 'kata 16';
    let valor = bestThing.slice(23,38);
    writeElement(id,valor);
    return JSON.stringify(valor);
}
kata16();


// Escreva uma função que use 'pop' para remover a última cidade de 'lotrCitiesArray e retorne o novo array.
// Lembre-se de também adicionar os resultados à página.

function kata17() {
    let id = 'kata 17';
    let valor = lotrCitiesArray.pop();
    writeElement(id,lotrCitiesArray);
    return JSON.stringify(lotrCitiesArray);
}
kata17();

// Escreva uma função que usa 'push' para adicionar de volta, no final do array, a cidade de 'lotrCitiesArray'
// que foi removida no #17 e retorne o novo array. Lembre-se de também adicionar os resultados à página.

function kata18() {
    let id = 'kata 18';
    let valor = lotrCitiesArray.push("Deadest Marshes");
    writeElement(id,lotrCitiesArray);
    return JSON.stringify(lotrCitiesArray);
}
kata18();


// Escreva uma função que usa 'shift' para remover a primeira cidade de 'lotrCitiesArray e retorne o novo 
//array. Lembre-se de também adicionar os resultados à página.

function kata19() {
    let id = 'kata 19';
    lotrCitiesArray.shift();
    writeElement(id,lotrCitiesArray);
    return JSON.stringify(lotrCitiesArray);
}
kata19();

// Escreva uma função que use 'unshift' para adicionar de volta, no começo do array, a cidade de 
//'lotrCitiesArray' que foi removida no #19 e retorne o novo array. Lembre-se de também adicionar os resultados à página.

function kata20() {
    let id = 'kata 20';
    lotrCitiesArray.unshift("Mordor")
    writeElement(id,lotrCitiesArray);
    return JSON.stringify(lotrCitiesArray);
}
kata20();

// Bonus//////////////////////////////////////////------------/////////////////////////////////////-----------/;
















// Escreva uma função que encontre e retorne o índice de 'only' em 'bestThing'. Lembre-se de também adicionar os resultados à página.
function katabonus1() {
    let id = 'kata bonus 1';
    let valor = bestThing.indexOf("only");
    writeElement(id,valor);
    return JSON.stringify(valor);
}
katabonus1();
// Escreva uma função que encontre e retorne o índice da última palavra de 'bestThing'. Lembre-se de também adicionar os resultados à página
function katabonus2() {
    let id = 'kata bonus 2';
    let valor = bestThing.lastIndexOf("bit");
    writeElement(id,valor);
    return JSON.stringify(valor);
}
katabonus2();
// Escreva uma função que encontre e retorne um array de todas as cidades de 'gotCitiesCSV' que tiverem vogais duplicadas ('aa', 'ee', etc.). Lembre-se de também adicionar os resultados à página.
let pattern = new RegExp(/(aa)|(ee)|(ii)|(oo)|(uu)/);

function katabonus3() {
    let id = 'kata bonus 3';
     let city = gotCitiesCSV.split(",");
       let valor = [];
       
    
       for (let i = 0; i <city.length; i++){
           if (pattern.test(city[i])){
               valor.push(city[i]);
           }writeElement(id,valor);
       }
 return JSON.stringify(valor);
}

// Escreva uma função que encontre e retorne um array com todas as cidades de 'lotrCitiesArray' que terminem em 'or'. Lembre-se de também adicionar os resultados à página.
function katabonus4() {
    let id = 'kata bonus 4';
    let valor = [];

    for(let i =0; i< lotrCitiesArray.length; i++) {
        if (lotrCitiesArray[i].endsWith("or")) {
            valor.push(lotrCitiesArray[i]);
        }
    }
    writeElement(id,valor);
return JSON.stringify(valor);
}
katabonus4();


// // Escreva uma função que encontre e retorne um array com todas as palavras de 'bestThing' começando com 'b'. Lembre-se de também adicionar os resultados à página
// function katabonus5() {
//     let id = 'kata bonus 5';
//     let best =  bestThing.split;
//     let valor = [];
//     for(let i = 0; i < best.length; i++) {
//         if (best[i].startsWith("b")) {
//             valor.push(best[i]);
//         }    
//     }
//     writeElement(id,valor);
//     return JSON.stringify(valor);
// }
// katabonus5();

// // Escreva uma função que retorne 'Sim' ou 'Não' se 'lotrCitiesArray' incluir 'Mirkwood'. Lembre-se de também adicionar os resultados à página.
// function katabonus6() {
//     let id = 'kata bonus 6';
//     if (lotrCitiesArray.includes("mirkwood")) {
//         writeElement(id,'Sim');
//         return JSON.stringify("sim");
//     }
//     else{
//         writeElement(id,'Não');
//         return JSON.stringify("não");
//     }
// }
// katabonus6();

// // Escreva uma função que retorne 'Sim' ou 'Não' se 'lotrCitiesArray' incluir 'Hollywood'. Lembre-se de também adicionar os resultados à página.
// function katabonus7() {
//     let id = 'kata bonus 7';
//     if(lotrCitiesArray.includes("Hollywood")) {
//         writeElement(id,'Sim');
//         return JSON.stringify("sim");
//     }
//     else {
//         writeElement(id,"Não");
//         return JSON.stringify("Não");
//     }
// }
// katabonus7();
// // Escreva uma função que retorne o índice de 'Mirkwood' em 'lotrCitiesArray'. Lembre-se de também adicionar os resultados à página.
// function katabonus8() {
// let id = 'kata bonus 8';
//     let valor = lotrCitiesArray.indexOf("Mirkwood");
//     writeElement(id,valor);
//     return JSON.stringify(valor);
// }
// katabonus8();
// // Escreva uma função que encontre e retorne a primeira cidade de 'lotrCitiesArray' que tiver mais de uma palavra. Lembre-se de também adicionar os resultados à página.
// function katabonus9() {
//     let id = 'kata bonus 9';
//     for(let i = 0; i< lotrCitiesArray.length; i++){
//         if (lotrCitiesArray[i].includes(" ")) {
//             writeElement(lotrCitiesArray[i]);
//             return JSON.stringify(id, lotrCitiesArray[i]);
//         }
//     }
// }
// katabonus9();

// // Escreva uma função que inverta a ordem de 'lotrCitiesArray' e retorne o novo array. Lembre-se de também adicionar os resultados à página.
// function katabonus10() {
//     let id = 'kata bonus 10';
//     let valor = lotrCitiesArray.reverse();
//     writeElement(id,valor);
//     return JSON.stringify(valor);

// }
// kataBonus10();


// // Escreva uma função que ordene 'lotrCitiesArray' alfabeticamente e retorne o novo array. Lembre-se de também adicionar os resultados à página.
// function katabonus11() {
//     let id = 'kata bonus 11';
//     let valor = lotrCitiesArray.sort();
//     writeElement(id,valor);
//     return JSON.stringify(valor);
// }
// kataBonus11();

// // Escreva uma função que ordene 'lotrCitiesArray' pelo número de caracteres em cada cidade (por exemplo, a cidade mais curta aparece primeiro) e retorne o novo array. Lembre-se de também adicionar os resultados à página.
// function katabonus12() {
//     let id = 'kata bonus 12';
//     let valor = lotrCitiesArray.sort((a,b) =>{
//         return a.length - b.length;
//     });
//     writeElement(id,valor);
//     return valor;
// }
// kataBonus12();

